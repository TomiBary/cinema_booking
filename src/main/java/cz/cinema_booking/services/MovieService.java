package cz.cinema_booking.services;


import cz.cinema_booking.models.Movie;
import cz.cinema_booking.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//https://github.com/Baeldung/spring-security-registration/tree/master/src/main/java/org/baeldung/service

@Service
public class MovieService {
  @Autowired
  private MovieRepository movieRepository;


  public List<Movie> getMovies() {
    List<Movie> movies = movieRepository.findAll();
    return movies;
  }

  public List<Movie> getMoviesByName(String name) {
    List<Movie> movies = movieRepository.findAll();
    List<Movie> moviesWithTheSameName = new ArrayList<>();
    for (int i = 0; i < movies.size(); i++) {
      if (movies.get(i).getName().equals(name))
        moviesWithTheSameName.add(movies.get(i));
    }
    return moviesWithTheSameName;
  }

  public Movie addMovie(Movie movie) {
    return movieRepository.save(movie);
  }
}
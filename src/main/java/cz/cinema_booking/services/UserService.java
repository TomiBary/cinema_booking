package cz.cinema_booking.services;

import cz.cinema_booking.models.Role;
import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.repositories.RoleRepository;
import cz.cinema_booking.repositories.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//https://github.com/Baeldung/spring-security-registration/tree/master/src/main/java/org/baeldung/service

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RoleRepository roleRepository;
  @Autowired
  private PasswordEncoder encoder;

  @Value("${enable.encoder}")
  private boolean isEncoderEnabled;

  @Transactional
  public SimpleUser registerNewUser(SimpleUser simpleUser) throws SecurityException {
    if (existsUserWithThisEmailOrUsername(simpleUser))
      throw new SecurityException("There is an account with that email address or username: " + simpleUser.getEmail());

    simpleUser.encodePassword(encoder);

//    Role role = null;
//    if (simpleUser.getUsername().equalsIgnoreCase("Admin")) {
//      Optional<Role> adminRoleOptional = roleRepository.findByType(Role.ADMIN.getType());
//      role = adminRoleOptional.orElseGet(() -> roleRepository.save(Role.ADMIN));
//    }
//    if (role == null) {
//      Optional<Role> userRoleOptional = roleRepository.findByType(Role.USER.getType());
//      role = userRoleOptional.orElseGet(() -> roleRepository.save(Role.USER));
//    }
//    simpleUser.withRole(role);

    return userRepository.save(simpleUser);
  }

  public void addRole(long userId, Role role, boolean add) {
    userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("User has not be foudn with ID: " + userId));
    Role roleNew;
    Optional<Role> roleOptional = roleRepository.findOne(Example.of(role));
    if (add) {
      if (!roleOptional.isPresent())
        roleNew = roleRepository.save(role);
      else
        roleNew = roleOptional.get();
      roleRepository.addRole(userId, roleNew.getId());
    } else {
      if (roleOptional.isPresent())
        roleRepository.deleteById(roleOptional.get().getId());
    }
  }

  private boolean existsUserWithThisEmailOrUsername(SimpleUser simpleUser) {
    List<SimpleUser> userList = userRepository.findByUsernameOrEmailAddress(simpleUser.getUsername(), simpleUser.getEmail());
    if (userList == null || userList.isEmpty()) return false;
    return true;
  }

  public List<SimpleUser> getUsers() {
    List<SimpleUser> users = userRepository.findAll();
    return users;
  }
}
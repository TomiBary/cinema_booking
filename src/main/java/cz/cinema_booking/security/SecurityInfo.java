package cz.cinema_booking.security;

public interface SecurityInfo{
	boolean isAdmin();

	boolean isUser();
//
//	boolean isUserFull();
//
//	boolean isLite();
//
//	boolean isApiUser();
//
////	default boolean hasSection(Sec section) {
////		return this.hasRole(section.getRole());
////	}
//
//	boolean hasRole(String var1);
//
//	boolean hasUnitRole(String var1, int var2);
//
//	boolean getShowBugger();
//
//	Localization getClientLocalization();
//
//	Localization getLocalization();
//
//	User getLoggedUser();
//
//	default MessagesMutationType getMuttationType() {
//		return MessagesMutationType.DEFAULT;
//	}
//
//	default void requireRole(String role) throws UserIllegalAccessException {
//		if (!this.hasRole(role)) {
//			throw new UserIllegalAccessException(this, (Authenticable)null, (Operation)null, role);
//		}
//	}
//
//	default void requireUnitRole(String role, int unitId) throws UserIllegalAccessException {
//		if (!this.hasUnitRole(role, unitId)) {
//			throw new UserIllegalAccessException(this, (Authenticable)null, (Operation)null, role);
//		}
//	}
//
//	Collection<? extends GrantedAuthority> getAuthorities();
//
//	static SecurityInfo createFakeSecurityInfo(Localization localization, User user, int clientId, int resellerId, int securityAgencyId) {
//		return new SecurityInfo.FakeSecurityInfo(localization, user, clientId, resellerId, securityAgencyId);
//	}
//
//	static SecurityInfo createAnonymous(Localization localization) {
//		return new SecurityInfo.AnonymousSecurityInfo(localization);
//	}
//
//	static SecurityInfo createReseller(Localization localization, User user) {
//		if (!user.isReseller()) {
//			throw new IllegalArgumentException("Given user " + user + " is not reseller");
//		} else {
//			return new SecurityInfo.FakeResellerSecurityInfo(localization, user);
//		}
//	}
//
//	public static class UserSecurityInfo implements SecurityInfo {
//		private User loggedUser;
//		private int clientId;
//		private int resellerId;
//
//		public UserSecurityInfo(User loggedUser, int clientId, int resellerId) {
//			this.loggedUser = loggedUser;
//			this.clientId = clientId;
//			this.resellerId = resellerId;
//		}
//
//		public int getSecurityAgencyId() {
//			return 0;
//		}
//
//		public boolean is(Auth auth) {
//			return this.loggedUser.is(auth);
//		}
//
//		public boolean isResellerIdAllEnabled() {
//			return this.loggedUser.getGrantedAccess().isResellerIdAllEnabled();
//		}
//
//		public boolean isCustomerIdAllEnabled() {
//			return this.loggedUser.getGrantedAccess().isCustomerIdAllEnabled();
//		}
//
//		public boolean isResellerIdSwitchEnabled() {
//			return this.loggedUser.getGrantedAccess().isResellerIdSwitchEnabled();
//		}
//
//		public boolean isCustomerIdSwitchEnabled() {
//			return this.loggedUser.getGrantedAccess().isCustomerIdSwitchEnabled();
//		}
//
//		public boolean isSecurityAgencySwitchEnabled() {
//			return this.loggedUser.getGrantedAccess().isSecurityAgencySwitchEnabled();
//		}
//
//		public boolean isResellerMultilogin() {
//			return this.loggedUser.getGrantedAccess().isResellerMultilogin();
//		}
//
//		public boolean isUserMultilogin() {
//			return this.loggedUser.getGrantedAccess().isUserMultilogin();
//		}
//
//		public boolean isRevokedRights() {
//			return this.loggedUser.getGrantedAccess().isRevokedRights();
//		}
//
//		public boolean hasUnit(int unitId) {
//			return this.loggedUser.getGrantedAccess().hasUnit(unitId);
//		}
//
//		public boolean isUserAdmin() {
//			return this.loggedUser.getGrantedAccess().isUserAdmin(this.clientId, this.resellerId);
//		}
//
//		public boolean isUserFull() {
//			return this.loggedUser.getGrantedAccess().isUserFull(this.clientId, this.resellerId);
//		}
//
//		public boolean isUser() {
//			return this.loggedUser.getGrantedAccess().isUser(this.clientId, this.resellerId);
//		}
//
//		public boolean isLite() {
//			return this.loggedUser.getGrantedAccess().isLite(this.clientId, this.resellerId);
//		}
//
//		public boolean isApiUser() {
//			return this.loggedUser.getGrantedAccess().isApiUser(this.clientId, this.resellerId);
//		}
//
//		public boolean hasRole(String role) {
//			return this.loggedUser.getGrantedAccess().hasRole(role, this.clientId, this.resellerId);
//		}
//
//		public boolean hasUnitRole(String role, int unitId) {
//			return this.loggedUser.getGrantedAccess().hasUnitRole(role, unitId);
//		}
//
//		public boolean getShowBugger() {
//			return false;
//		}
//
//		public Localization getClientLocalization() {
//			return this.getLocalization();
//		}
//
//		public Localization getLocalization() {
//			return this.loggedUser.getLocalization();
//		}
//
//		public Collection<? extends GrantedAuthority> getAuthorities() {
//			return this.loggedUser.getGrantedAccess().getAllRoles(this.clientId, this.resellerId);
//		}
//
//		public User getLoggedUser() {
//			return this.loggedUser;
//		}
//
//		public int getClientId() {
//			return this.clientId;
//		}
//
//		public int getResellerId() {
//			return this.resellerId;
//		}
//
//		public void setLoggedUser(User loggedUser) {
//			this.loggedUser = loggedUser;
//		}
//
//		public void setClientId(int clientId) {
//			this.clientId = clientId;
//		}
//
//		public void setResellerId(int resellerId) {
//			this.resellerId = resellerId;
//		}
//
//		public boolean equals(Object o) {
//			if (o == this) {
//				return true;
//			} else if (!(o instanceof SecurityInfo.UserSecurityInfo)) {
//				return false;
//			} else {
//				SecurityInfo.UserSecurityInfo other = (SecurityInfo.UserSecurityInfo)o;
//				if (!other.canEqual(this)) {
//					return false;
//				} else {
//					label31: {
//						Object this$loggedUser = this.getLoggedUser();
//						Object other$loggedUser = other.getLoggedUser();
//						if (this$loggedUser == null) {
//							if (other$loggedUser == null) {
//								break label31;
//							}
//						} else if (this$loggedUser.equals(other$loggedUser)) {
//							break label31;
//						}
//
//						return false;
//					}
//
//					if (this.getClientId() != other.getClientId()) {
//						return false;
//					} else {
//						return this.getResellerId() == other.getResellerId();
//					}
//				}
//			}
//		}
//
//		protected boolean canEqual(Object other) {
//			return other instanceof SecurityInfo.UserSecurityInfo;
//		}
//
//		public int hashCode() {
//			int PRIME = true;
//			int result = 1;
//			Object $loggedUser = this.getLoggedUser();
//			int result = result * 59 + ($loggedUser == null ? 43 : $loggedUser.hashCode());
//			result = result * 59 + this.getClientId();
//			result = result * 59 + this.getResellerId();
//			return result;
//		}
//
//		public String toString() {
//			return "SecurityInfo.UserSecurityInfo(loggedUser=" + this.getLoggedUser() + ", clientId=" + this.getClientId() + ", resellerId=" + this.getResellerId() + ")";
//		}
//	}
//
//	public static class AnonymousSecurityInfo implements SecurityInfo {
//		protected Localization localization;
//
//		public Localization getClientLocalization() {
//			return this.localization;
//		}
//
//		public boolean is(Auth auth) {
//			return false;
//		}
//
//		public boolean isResellerIdAllEnabled() {
//			return false;
//		}
//
//		public boolean isCustomerIdAllEnabled() {
//			return false;
//		}
//
//		public boolean isResellerIdSwitchEnabled() {
//			return false;
//		}
//
//		public boolean isCustomerIdSwitchEnabled() {
//			return false;
//		}
//
//		public boolean isSecurityAgencySwitchEnabled() {
//			return false;
//		}
//
//		public boolean isResellerMultilogin() {
//			return false;
//		}
//
//		public boolean isUserMultilogin() {
//			return false;
//		}
//
//		public boolean isRevokedRights() {
//			return false;
//		}
//
//		public boolean isUserAdmin() {
//			return false;
//		}
//
//		public boolean isUser() {
//			return false;
//		}
//
//		public boolean isUserFull() {
//			return false;
//		}
//
//		public boolean isLite() {
//			return false;
//		}
//
//		public boolean hasUnitRole(String authorityStr, int unitId) {
//			return false;
//		}
//
//		public boolean hasUnit(int unitId) {
//			return false;
//		}
//
//		public boolean hasRole(String role) {
//			return false;
//		}
//
//		public User getLoggedUser() {
//			return null;
//		}
//
//		public int getResellerId() {
//			return 0;
//		}
//
//		public int getClientId() {
//			return 0;
//		}
//
//		public int getSecurityAgencyId() {
//			return 0;
//		}
//
//		public Collection<? extends GrantedAuthority> getAuthorities() {
//			return Collections.emptyList();
//		}
//
//		public boolean getShowBugger() {
//			return false;
//		}
//
//		public boolean isApiUser() {
//			return false;
//		}
//
//		AnonymousSecurityInfo(Localization localization) {
//			this.localization = localization;
//		}
//
//		public Localization getLocalization() {
//			return this.localization;
//		}
//	}
//
//	public static class FakeSecurityInfo extends SecurityInfo.AnonymousSecurityInfo implements SecurityInfo {
//		private User loggedUser;
//		private int clientId;
//		private int resellerId;
//		private int securityAgencyId;
//
//		FakeSecurityInfo(Localization localization, User loggedUser, int clientId, int resellerId, int securityAgencyId) {
//			super(localization);
//			this.loggedUser = loggedUser;
//			this.clientId = clientId;
//			this.resellerId = resellerId;
//			this.securityAgencyId = securityAgencyId;
//		}
//
//		public boolean isUser() {
//			return true;
//		}
//
//		public boolean hasUnitRole(String authorityStr, int unitId) {
//			return true;
//		}
//
//		public boolean hasUnit(int unitId) {
//			return true;
//		}
//
//		public boolean hasRole(String role) {
//			return true;
//		}
//
//		public User getLoggedUser() {
//			return this.loggedUser;
//		}
//
//		public int getClientId() {
//			return this.clientId;
//		}
//
//		public int getResellerId() {
//			return this.resellerId;
//		}
//
//		public int getSecurityAgencyId() {
//			return this.securityAgencyId;
//		}
//
//		public void setLoggedUser(User loggedUser) {
//			this.loggedUser = loggedUser;
//		}
//
//		public void setClientId(int clientId) {
//			this.clientId = clientId;
//		}
//
//		public void setResellerId(int resellerId) {
//			this.resellerId = resellerId;
//		}
//
//		public void setSecurityAgencyId(int securityAgencyId) {
//			this.securityAgencyId = securityAgencyId;
//		}
//
//		public boolean equals(Object o) {
//			if (o == this) {
//				return true;
//			} else if (!(o instanceof SecurityInfo.FakeSecurityInfo)) {
//				return false;
//			} else {
//				SecurityInfo.FakeSecurityInfo other = (SecurityInfo.FakeSecurityInfo)o;
//				if (!other.canEqual(this)) {
//					return false;
//				} else {
//					Object this$loggedUser = this.getLoggedUser();
//					Object other$loggedUser = other.getLoggedUser();
//					if (this$loggedUser == null) {
//						if (other$loggedUser != null) {
//							return false;
//						}
//					} else if (!this$loggedUser.equals(other$loggedUser)) {
//						return false;
//					}
//
//					if (this.getClientId() != other.getClientId()) {
//						return false;
//					} else if (this.getResellerId() != other.getResellerId()) {
//						return false;
//					} else if (this.getSecurityAgencyId() != other.getSecurityAgencyId()) {
//						return false;
//					} else {
//						return true;
//					}
//				}
//			}
//		}
//
//		protected boolean canEqual(Object other) {
//			return other instanceof SecurityInfo.FakeSecurityInfo;
//		}
//
//		public int hashCode() {
//			int PRIME = true;
//			int result = 1;
//			Object $loggedUser = this.getLoggedUser();
//			int result = result * 59 + ($loggedUser == null ? 43 : $loggedUser.hashCode());
//			result = result * 59 + this.getClientId();
//			result = result * 59 + this.getResellerId();
//			result = result * 59 + this.getSecurityAgencyId();
//			return result;
//		}
//
//		public String toString() {
//			return "SecurityInfo.FakeSecurityInfo(loggedUser=" + this.getLoggedUser() + ", clientId=" + this.getClientId() + ", resellerId=" + this.getResellerId() + ", securityAgencyId=" + this.getSecurityAgencyId() + ")";
//		}
//	}
//
//	public static class FakeResellerSecurityInfo extends SecurityInfo.FakeSecurityInfo {
//		FakeResellerSecurityInfo(Localization localization, User loggedUser) {
//			super(localization, loggedUser, 0, loggedUser.getDefaultResellerId(), 0);
//		}
//
//		public boolean isUser() {
//			return false;
//		}
//
//		public boolean isReseller() {
//			return true;
//		}
//	}
}


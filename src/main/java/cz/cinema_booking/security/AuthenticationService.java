package cz.cinema_booking.security;

import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.repositories.UserRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements UserDetailsService {
  @Autowired
  private UserRepository userRepository;

  //returns actual logged user
  public static UserDetailsImpl getUserInfo(HttpServletRequest request) {
    HttpSession session = request.getSession(false);
    return getUserInfo(session);
  }

  //TODO move methods to different class

  public static UserDetailsImpl getUserInfo(HttpSession session) {
    if (session == null) return null;
    SecurityContext context = (SecurityContext) session.getAttribute("SPRING_SECURITY_CONTEXT");
    if (context == null) return null;
    return getUserInfo(context.getAuthentication());
  }

  public static UserDetailsImpl getUserInfo() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    return getUserInfo(auth);
  }

  public static UserDetailsImpl getUserInfo(Authentication auth) {
    if (auth == null || auth instanceof AnonymousAuthenticationToken) return null;
    if (auth.getPrincipal() instanceof User)
      return new UserDetailsImpl((User) auth.getPrincipal());
    if (UserDetailsImpl.class.isAssignableFrom(auth.getPrincipal().getClass()))
      return (UserDetailsImpl) auth.getPrincipal();
    return null;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDetailsImpl loggedUser = getUserInfo();
    if (loggedUser != null) throw new SecurityException("User already logged");
    SimpleUser simpleUser = userRepository.findByUsername(username);
    if (simpleUser == null) throw new SecurityException("User Not found");
    return new UserDetailsImpl(simpleUser);
  }
}

package cz.cinema_booking.security;

import cz.cinema_booking.models.Role;
import cz.cinema_booking.models.SimpleUser;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@NoArgsConstructor
public class UserDetailsImpl implements UserDetails {
  //TODO Extends spring User class instead implementing UserDetails

  @Setter(AccessLevel.NONE)
  @Getter(AccessLevel.NONE)
  private SimpleUser user;
  private String username;
  private String password;
  private Collection<? extends GrantedAuthority> roles;

  public UserDetailsImpl(SimpleUser simpleUser) {
    this.user = simpleUser;
    this.username = simpleUser.getUsername();
    this.password = simpleUser.getPassword();
    this.roles = simpleUser.getRoles();
  }

  public UserDetailsImpl(User user) {
    this.username = user.getUsername();
    this.password = user.getPassword();
    this.roles = user.getAuthorities();
  }

  public long getUserId() {
    //TODO FUJ !!!
    return user != null ? user.getId() : 0;
  }

  public boolean isAdmin() {
    return user.getRoles().stream().anyMatch(r -> r.getType() == Role.RoleType.ADMIN);
  }

  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public String toString() {
    return "UserDetailsImpl{" + "username='" + username + '\'' + ", password='" + password + '\'' + ", authList=" + roles + '}';
  }
}

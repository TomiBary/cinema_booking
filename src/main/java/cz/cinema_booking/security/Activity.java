package cz.cinema_booking.security;


import cz.cinema_booking.utils.UserAgentParser;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Activity {
	private Type type;
	private String ip;
	private String description;
	private Date inserted;


	/**For saving data about activity */
	private Activity(UserDetailsImpl userDetailsImpl, Type type){
//		this.user = userInfo.getLoggedUser();
//		Integer adminUserId = userInfo.getAdminUserId();
//		if (adminUserId != null) this.admin = new User(adminUserId); // fake user to save in db
		this.type = type;
	}

	private Activity(UserDetailsImpl userDetailsImpl, String ip, String description){
//		this.user = loginResult.getUser();
//		this.admin = loginResult.getAdmin();
		this.type = Type.LOGIN;
		this.ip = ip;
		this.description = description;
	}

	public static Activity getLogin(UserDetailsImpl userDetailsImpl, HttpServletRequest request) {
		UserAgentParser parser = new UserAgentParser(request.getHeader("User-Agent"));
		String description = parser.getBrowserName() + " " + parser.getBrowserVersion() + "; " + parser.getBrowserOperatingSystem();
		return new Activity(userDetailsImpl, request.getRemoteAddr(), description);
	}

	public static Activity getLogout(UserDetailsImpl userDetailsImpl, boolean sessionTimeout) {
		return new Activity(userDetailsImpl, !sessionTimeout ? Type.LOGOUT : Type.SESSION_TIMEOUT);
	}

	public enum Type{
		LOGIN(1), LOGOUT(2), SESSION_TIMEOUT(3);

		private byte id;

		Type(int id){
			this.id = (byte) id;
		}

		public Byte getId(){
			return id;
		}

		public static Type valueOf(Number id){
			return Arrays.stream(values()).filter(t->t.getId().equals(id.byteValue())).findFirst().orElse(null);
		}

	}
}

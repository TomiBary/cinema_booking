package cz.cinema_booking.repositories;

import cz.cinema_booking.models.ProjectionTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectionTimeRepository extends JpaRepository<ProjectionTime, Long> {
}
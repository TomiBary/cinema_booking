package cz.cinema_booking.repositories;


import cz.cinema_booking.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByType(Role.RoleType type);

	@Query(value = "INSERT INTO user_role(user_id, role_id) VALUES (?2, ?1)", nativeQuery = true)
	void addRole(long roleId, long userId);
}
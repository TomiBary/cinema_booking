package cz.cinema_booking.repositories;


import cz.cinema_booking.models.SimpleUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<SimpleUser, Long> {
    List<SimpleUser> findUserById(Long userId);
    List<SimpleUser> findByEmailAddress(String emailAddress);
    SimpleUser findByUsername(String username);

    List<SimpleUser> findByUsernameOrEmailAddress(String username, String emailAddress);
}
package cz.cinema_booking.repositories;


import cz.cinema_booking.models.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {
    //SELECT s from seat s inner join reservation_seat rs on s.id = rs.seat_id inner join reservation r on r.id = rs.reservation_id inner join projection_time p on p.id = r.projection_time_id inner join hall h on h.id = p.hall_id WHERE h.id = ?1 and p.id = ?2
		@Query(value = "SELECT s.*, rs.seat_id as reservationSeatId, r.id as reservationId, p.hall_id as hallId, p.projection_date_time as startTime, h.name as hallName " +
				"FROM seat s " +
				"INNER JOIN reservation_seat rs ON s.id = rs.seat_id " +
				"INNER JOIN reservation r ON r.id = rs.reservation_id " +
				"INNER JOIN projection_time p ON p.id = r.projection_time_id " +
				"INNER JOIN hall h ON h.id = p.hall_id " +
				"WHERE p.id = ?1", nativeQuery = true)

		List<Seat> findAllUnavailableForProjection(Long projectionTimeId);

	List<Seat> findAllByHallId(Long hallId);
//
//    @Query("SELECT s FROM reservation r JOIN r.")
//    List<Seat> findAllUnavailable2(Long projectionTimeId);

}

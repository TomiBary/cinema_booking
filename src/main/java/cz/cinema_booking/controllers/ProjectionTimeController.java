package cz.cinema_booking.controllers;

import cz.cinema_booking.models.ProjectionTime;
import cz.cinema_booking.repositories.HallRepository;
import cz.cinema_booking.repositories.MovieRepository;
import cz.cinema_booking.repositories.ProjectionTimeRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/projection")
public class ProjectionTimeController {

  @Autowired
  ProjectionTimeRepository projectionTimeRepository;
  @Autowired
  MovieRepository movieRepository;
  @Autowired
  HallRepository hallRepository;

  @GetMapping(value = {"", "/list"})
  public String list(Model model, Principal p) {
    List<ProjectionTime> projections = projectionTimeRepository.findAll();
    model.addAttribute("projectionList", projections);
    return "/projection/list";
  }

  @GetMapping(value = "/view")
  public ModelAndView viewGet(@RequestParam(name = "id") long id) {
    Optional<ProjectionTime> projectionTime = projectionTimeRepository.findById(id);
    if (!projectionTime.isPresent())
      throw new IllegalArgumentException("ProjectionTime with id: " + id + " could not be found.");
    return new ModelAndView("/projection/view", "projection", projectionTime.get());
  }

  @GetMapping(value = "/upsert")
  @PreAuthorize("hasRole('ADMIN')")
  public String upsertGet(@RequestParam(name = "id", defaultValue = "0") long id, Model model, Principal p) {
    //TODO only ADMIN

    if (id <= 0) {
      model.addAttribute("projection", new ProjectionTime());
    } else {
      Optional<ProjectionTime> projectionTime = projectionTimeRepository.findById(id);
      if (!projectionTime.isPresent())
        throw new IllegalArgumentException("ProjectionTime with id: " + id + " could not be found.");
      model.addAttribute("projection", projectionTime.get());
    }
    model.addAttribute("movies", movieRepository.findAll());
    model.addAttribute("halls", hallRepository.findAll());
    return "/projection/upsert";
  }

  @PostMapping(value = "/upsert")
  @PreAuthorize("hasRole('ADMIN')")
  public ModelAndView upsert(@Valid @ModelAttribute("projection") ProjectionTime projectionTime, Principal p, BindingResult result) {
    //TODO only ADMIN

    if (result.hasErrors()) {
      return new ModelAndView("/projection/upsert", "projection", projectionTime);
    } else {
      ProjectionTime projection = projectionTimeRepository.save(projectionTime);
//            return new ModelAndView("redirect:/projection/view", "id", projection.getId());
      return new ModelAndView("redirect:/projection/list");
    }
  }

  @GetMapping(value = "/delete")
  @PreAuthorize("hasRole('ADMIN')")
  public String delete(@RequestParam(name = "id") long id) {
    //TODO only ADMIN

    projectionTimeRepository.deleteById(id);
    System.out.println("Exist projection with id?: " + id + " projection: " + projectionTimeRepository.existsById(id));
    return "redirect:/projection/list";
  }
}
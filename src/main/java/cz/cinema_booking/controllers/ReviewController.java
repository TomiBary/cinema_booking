package cz.cinema_booking.controllers;

import cz.cinema_booking.models.Review;
import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.repositories.ReviewRepository;
import cz.cinema_booking.security.UserDetailsImpl;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/review")
public class ReviewController {
  final ReviewRepository reviewRepository;

  public ReviewController(ReviewRepository reviewRepository) {
    this.reviewRepository = reviewRepository;
  }

  @GetMapping(value = {"", "/list"})
  public String list(Model model, Principal p) {
    List<Review> reviews = reviewRepository.findAll();
    model.addAttribute("reviewList", reviews);
    System.out.println(reviews);
    return "/review/list";
  }

  @GetMapping(value = "/view")
  public ModelAndView viewGet(@RequestParam(name = "id") long id) {
    Optional<Review> review = reviewRepository.findById(id);
    if (review.isEmpty())
      throw new IllegalArgumentException("Review with id: " + id + " could not be found.");
    return new ModelAndView("/review/view", "review", review.get());
  }

  @GetMapping(value = "/upsert")
  @PreAuthorize("hasRole('ADMIN')")
  public String upsertGet(@RequestParam(name = "id", defaultValue = "0") long id, Model model) {
    //TODO only ADMIN
    if (id <= 0) {
      model.addAttribute("review", new Review());
    } else {
      Review review = reviewRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Review with id: " + id + " could not be found."));

      model.addAttribute("review", review);
    }
    return "/review/upsert";
  }

  @PostMapping(value = "/upsert")
  @PreAuthorize("hasRole('ADMIN')")
  public ModelAndView upsert(@Valid @ModelAttribute("review") Review reviewNew, Model model, UserDetailsImpl userDetails, BindingResult result) {
    //TODO only ADMIN
    if (result.hasErrors()) {
      return new ModelAndView("/review/upsert", "review", reviewNew);
    } else {
      reviewNew.setAuthor(new SimpleUser().withId(userDetails.getUserId()));
      Review review = reviewRepository.save(reviewNew);
      return new ModelAndView("redirect:/review/view", "id", review.getId());
    }
  }

  @GetMapping(value = "/delete")
  @PreAuthorize("hasRole('ADMIN')")
  public String delete(@RequestParam(name = "id") long id) {
    //TODO only ADMIN

    reviewRepository.deleteById(id);
    System.out.println("Exist article with id?: " + id + " article: " + reviewRepository.existsById(id));
    return "redirect:/review/list";
  }
}
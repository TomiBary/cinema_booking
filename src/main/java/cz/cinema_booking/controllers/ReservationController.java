package cz.cinema_booking.controllers;

import cz.cinema_booking.models.ProjectionTime;
import cz.cinema_booking.models.Reservation;
import cz.cinema_booking.models.ReservationForm;
import cz.cinema_booking.models.Seat;
import cz.cinema_booking.repositories.ProjectionTimeRepository;
import cz.cinema_booking.repositories.ReservationRepository;
import cz.cinema_booking.repositories.SeatRepository;
import cz.cinema_booking.security.UserDetailsImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
@RequestMapping(value = "/reservation")
public class ReservationController {
  @Autowired
  ReservationRepository reservationRepository;
  @Autowired
  ProjectionTimeRepository projectionTimeRepository;
  @Autowired
  SeatRepository seatRepository;

  @GetMapping(value = {"", "/list"})
  public String list(Model model, UserDetailsImpl userDetails) {
    List<Reservation> reservations;
    if (userDetails.isAdmin()) {
      reservations = reservationRepository.findAll();
    } else {
      reservations = reservationRepository.findAllByOwnerId(userDetails.getUserId());
    }
    model.addAttribute("reservationList", reservations);
    return "/reservation/list";
  }

  @GetMapping(value = "/view")
  public ModelAndView viewGet(@RequestParam(name = "id") long id) {
    Optional<Reservation> reservation = reservationRepository.findById(id);
    if (!reservation.isPresent())
      throw new IllegalArgumentException("Reservation with id: " + id + " could not be found.");
    return new ModelAndView("/reservation/view", "reservation", reservation.get());
  }

  @GetMapping(value = "/create")
  public ModelAndView create(@RequestParam(name = "projection_id", defaultValue = "0") long projectionId, Model model) {
    ProjectionTime projectionTime = prepareReservation(projectionId, model);
    return new ModelAndView("/reservation/upsert", "reservationForm", new ReservationForm(projectionTime.getId().intValue(), null));
  }

  private ProjectionTime prepareReservation(long projectionId, Model model) {
    ProjectionTime projectionTime = projectionTimeRepository.findById(projectionId).orElseThrow(
        () -> new IllegalArgumentException("Projection was not found with id: " + projectionId));

    List<Seat> allUnavailableSeats = seatRepository.findAllUnavailableForProjection(projectionId);
    List<Seat> allHallSeats = seatRepository.findAllByHallId(projectionTime.getHall().getId());
    Map<Integer, List<Seat>> map = Seat.getSeatMap(allHallSeats);

    List<List<Seat>> seatListList = new ArrayList<>();
    for (Map.Entry<Integer, List<Seat>> entry : map.entrySet()) {
      System.out.println("Entry key: " + entry.getKey());
      seatListList.add(entry.getValue());
    }
    Collections.reverse(seatListList);

    model.addAttribute("unavailableSeats", allUnavailableSeats);
    model.addAttribute("seatListList", seatListList);
    model.addAttribute("reservation", new Reservation(projectionTime));
    return projectionTime;
  }

  @GetMapping(value = "/upsert")
  public String upsertGet(@RequestParam(name = "id", defaultValue = "0") long id, Model model) {
    //TODO only ADMIN
    if (id <= 0) {
      model.addAttribute("reservation", new Reservation());
    } else {
      Optional<Reservation> reservation = reservationRepository.findById(id);
      if (!reservation.isPresent())
        throw new IllegalArgumentException("Reservation with id: " + id + " could not be found.");
      model.addAttribute("reservation", reservation.get());
    }
    return "/reservation/upsert";
  }


  @PostMapping(value = "/upsert")
  public ModelAndView upsert(@Valid @ModelAttribute("reservationForm") ReservationForm form, UserDetailsImpl userDetails, Model model, BindingResult result) {
    //TODO only ADMIN
    long projectionId = form.getProjectionId();
    projectionTimeRepository.findById(projectionId).orElseThrow(() ->
        new IllegalArgumentException("Cannot create reservation for projectionTime with id: " + projectionId));

    form.withOnlyEnabled();
    System.out.println(form.getSeatHelperList());

    Collection<Seat> seats = form.getSeats();
    Reservation reservation = form.toReservation()
        .withOwnerId(userDetails.getUserId())
        .withSeats(seats);
    List<Seat> unavailableSeats = seatRepository.findAllUnavailableForProjection(projectionId);
    unavailableSeats = unavailableSeats.stream().distinct().filter(s -> reservation.getSeats().stream().anyMatch(rs -> rs.getId() == s.getId())).collect(Collectors.toList());
    if (unavailableSeats.size() > 0)
      result.reject("seatReserved", "One of chosen seats is already reserved");
    if (result.hasErrors()) {
      //TODO show which seats are already reserved
//            unavailableSeats.forEach(s ->); //need to get boxId from form object
      ProjectionTime projectionTime = prepareReservation(projectionId, model);
      return new ModelAndView("/reservation/upsert", "reservationForm", new ReservationForm(projectionTime.getId().intValue(), null));
    } else {
      Reservation reservationNew = reservationRepository.save(reservation);
      return new ModelAndView("redirect:/reservation/view", "id", reservationNew.getId());
    }
  }

  @GetMapping(value = "/delete")
  @PreAuthorize("hasRole('ADMIN')")
  public String delete(@RequestParam(name = "id") long id) {
    //TODO only ADMIN

    reservationRepository.deleteById(id);
    System.out.println("Exist reservation with id?: " + id + " reservation: " + reservationRepository.existsById(id));
    return "redirect:/reservation/list";
  }
}
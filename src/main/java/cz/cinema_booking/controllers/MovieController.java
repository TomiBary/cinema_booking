package cz.cinema_booking.controllers;

import cz.cinema_booking.models.Movie;
import cz.cinema_booking.repositories.MovieRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/movie")
public class MovieController {

    final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @GetMapping(value = {"","/list"})
    public String list(Model model, Principal p) {
        List<Movie> movies = movieRepository.findAll();
        model.addAttribute("movieList", movies);
        return "/movie/list";
    }

    @GetMapping(value = "/view")
    public ModelAndView viewGet(@RequestParam(name = "id") long id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if(!movie.isPresent())
            throw new IllegalArgumentException("Movie with id: " + id + " could not be found.");
        return new ModelAndView("/movie/view", "movie", movie.get());
    }

    @GetMapping(value = "/upsert")
    @PreAuthorize("hasRole('ADMIN')")
    public String upsertGet(@RequestParam(name = "id", defaultValue = "0") long id, Model model, Principal p) {
        //TODO only ADMIN
        if (id <= 0) {
            model.addAttribute("movie", new Movie());
        } else {
            Optional<Movie> movie = movieRepository.findById(id);
            if(!movie.isPresent())
                throw new IllegalArgumentException("Movie with id: " + id + " could not be found.");
            model.addAttribute("movie", movie.get());
        }
        return "/movie/upsert";
    }

    @PostMapping(value = "/upsert")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView upsert(@ModelAttribute("movie") Movie movieNew, Principal p, BindingResult result) {
        //TODO only ADMIN

        if (result.hasErrors()){
            return new ModelAndView("/movie/upsert", "movie", movieNew);
        }else{
            Movie movie = movieRepository.save(movieNew);
            return new ModelAndView("redirect:/movie/view", "id", movie.getId());
        }
    }

    @GetMapping(value = "/delete")
    @PreAuthorize("hasRole('ADMIN')")
    public String delete(@RequestParam(name = "id") long id) {
        //TODO only ADMIN

        movieRepository.deleteById(id);
        System.out.println("Exist movie with id?: " + id + " movie: " + movieRepository.existsById(id));
        return "redirect:/movie/list";
    }
}
package cz.cinema_booking.controllers;

import cz.cinema_booking.models.Role;
import cz.cinema_booking.models.RoleForm;
import cz.cinema_booking.models.RoleHelper;
import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.repositories.RoleRepository;
import cz.cinema_booking.repositories.UserRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/admin/role")
public class RoleController {

  final UserRepository userRepository;
  final RoleRepository roleRepository;

  public RoleController(UserRepository userRepository, RoleRepository roleRepository) {
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
  }

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping(value = {"", "/list"})
  public String list(Model model, Principal p) {
    prepareModel(model);
    return "admin/role/list";
  }

  @PreAuthorize("hasRole('ADMIN')")
  @RequestMapping(value = "/upsert")  //state = true -> ma mit tuto roli
  public String upsert(@ModelAttribute RoleForm roleForm, Principal p, Model model) {
    //TODO only ADMIN
    long userId = roleForm.getUserId();
    if (userId > 0 && !roleForm.getRoleHelperList().isEmpty()) {
      SimpleUser user = userRepository.findById(userId).orElseThrow(() -> new IllegalArgumentException("User not found with id: " + userId));

      for (RoleHelper roleHelper : roleForm.getRoleHelperList()) {
        long roleId = roleHelper.getRoleId();
        Role role = roleRepository.findById(roleId).orElseThrow(() -> new IllegalArgumentException("Role not found with id: " + roleId));
        //if has role and wants to remove it
        if (user.getRoles().contains(role) && !roleHelper.isEnabled())
          userRepository.save(user.withoutRole(role));
          //if doesnt have role and wants to add it
        else if (!user.getRoles().contains(role) && roleHelper.isEnabled())
          userRepository.save(user.withRole(role));
      }
    }
    prepareModel(model);
    return "redirect:/admin/role/";
  }

  private void prepareModel(Model model) {
    List<SimpleUser> users = userRepository.findAll();
    List<Role> roles = roleRepository.findAll();
    model.addAttribute("roleForm", new RoleForm());
    model.addAttribute("users", users);
    model.addAttribute("roles", roles);
  }
}
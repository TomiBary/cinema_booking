package cz.cinema_booking.controllers;

import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController {

  @Autowired
  private UserService userService;

  @GetMapping
  public String getIndex(Model model) {
    model.addAttribute("simpleUser", new SimpleUser());
    return "registration/registration";
  }

  @PostMapping
//    , WebRequest request, Errors errors
  public ModelAndView registerUserAccount(@ModelAttribute("simpleUser") @Valid SimpleUser simpleUser, BindingResult result) {

    SimpleUser registered;
    if (!result.hasErrors()) {
      try {
        registered = userService.registerNewUser(simpleUser);
        if (registered == null) {
          result.rejectValue("email", "internal error");
        }
      } catch (SecurityException ex) {
        result.reject("userExists", "User with same username or email already exists");
      }
    }

    if (result.hasErrors()) {
      return new ModelAndView("registration/registration", "user", simpleUser);
    } else {
      return new ModelAndView("redirect:/");
    }
  }

}
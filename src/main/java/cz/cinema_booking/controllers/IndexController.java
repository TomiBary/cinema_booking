package cz.cinema_booking.controllers;

import cz.cinema_booking.models.*;
import cz.cinema_booking.repositories.*;
import cz.cinema_booking.security.UserDetailsImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class IndexController {

  private final MovieRepository movieRepository;
  private final ProjectionTimeRepository projectionRepository;
  private final SeatRepository seatRepository;
  private final HallRepository hallRepository;
  private final UserRepository userRepository;

  public IndexController(MovieRepository movieRepository, ProjectionTimeRepository projectionRepository, SeatRepository seatRepository, HallRepository hallRepository, UserRepository userRepository) {
    this.movieRepository = movieRepository;
    this.projectionRepository = projectionRepository;
    this.seatRepository = seatRepository;
    this.hallRepository = hallRepository;
    this.userRepository = userRepository;
  }

//  @GetMapping(value = {"/", "/index"})
//  public String index(Model model, UserDetailsImpl userDetailsImpl) {
//    System.out.println("UserDetailsImpl: " + userDetailsImpl);
//
//    //TODO dostat nejnovejsi movie
//    Movie newestMovie = movieRepository.getOne((long) 5);
//    model.addAttribute("newestMovie", newestMovie);
//    return "index/index";
//  }

  @GetMapping(value = {"/init"})
  public String init(Model model, UserDetailsImpl userDetailsImpl) {
    System.out.println("APP INITIATION");

    //                                                                                            hashed password with bCrypt: admin
    userRepository.save(new SimpleUser("admin", "admin@admin.cz", "$2a$12$88R/DZPwpLNEHec9LjbNZOur.2dbYdYRZVZetLNoXp/IvEaQ6dPoq", null, null));
//    userRepository.save(new SimpleUser("user", "user@admin.cz", "user", null, null));

    System.out.println("USERS CREATED");

    Hall hall = new Hall("Mistnost 1");
    if (hallRepository.count() == 0) {
      hall = hallRepository.save(hall);
      System.out.println("CREATED HALL");

      if (!(movieRepository.count() > 0)) {
        for (int i = 0; i < 5; i++) {

          List<ProjectionTime> times = new ArrayList<>(3);
          Movie movie = movieRepository.save(new Movie("Movie " + (i + 1), 90 * 60, "Popis filmu, o cem je herci atp?", new Date(), null));
          for (int j = 0; j < 3; j++) {
            times.add(new ProjectionTime(movie, new Date(), hall, null));
          }
          projectionRepository.saveAll(times);
        }
        System.out.println("CREATED MOVIES & PROJECTIONS");
      }

      //Generate seats
      if (seatRepository.findAll().isEmpty()) {
        List<Seat> seats = new ArrayList<>();
        for (int i = 1; i <= 11; i++) {
          for (int j = 1; j <= 20; j++) {
            seats.add(new Seat(Coordinates.getCharForNumber(i) + j, i, j, hall.getId().intValue()));
          }
        }
        seatRepository.saveAll(seats);
        System.out.println("CREATED SEATS");
      }
    }
    return "index";
  }
}


package cz.cinema_booking.utils;

import cz.cinema_booking.security.AuthenticationService;
import cz.cinema_booking.security.UserDetailsImpl;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * Helper class, that takes UserInfo from request, and allows this object to be injected into controller method
 */
@Component
public class UserDetailsArgumentResolver implements HandlerMethodArgumentResolver {
  @Override
  public boolean supportsParameter(MethodParameter parameter) {
    return parameter.getParameterType().equals(UserDetailsImpl.class);
  }

  @Override
  public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
                                WebDataBinderFactory binderFactory) throws Exception {
    HttpServletRequest httpRequest = webRequest.getNativeRequest(HttpServletRequest.class);
    if (httpRequest == null)
      throw new Exception("Unknown native request class :" + webRequest.getNativeRequest().getClass());
    return AuthenticationService.getUserInfo(httpRequest);
  }
}

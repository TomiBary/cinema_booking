package cz.cinema_booking.utils;

import cz.cinema_booking.security.*;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RequestUtilities {
	/**
	 * Can be used to access current {@link HttpServletRequest}
	 * But use only when necessary!!!
	 * @return
	 */
	public static HttpServletRequest getCurrentHttpRequest(){
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes instanceof ServletRequestAttributes) {
			return ((ServletRequestAttributes)requestAttributes).getRequest();
		}
		return null;
	}

	/**
	 * @return current options from request ,use from validator
	 */
	public static UserDetailsImpl getCurrentUserInfo(){
		AuthenticationService authService = SpringContext.getBean(AuthenticationService.class);
		return authService.getUserInfo(getCurrentHttpRequest());
	}



//
//
//	@Deprecated
//	public static String getLanguage(HttpServletRequest httpServletRequest) {
//		return getLocale(httpServletRequest).getLanguage();
//	}
//
//	public static Locale getLocale(HttpServletRequest httpServletRequest) {
//		return RequestContextUtils.getLocale(httpServletRequest);
//	}
//
//	public static Locale getCompatibleLocale(HttpServletRequest request, List<String> possibleLanguageList) {
//		Locale userOrSessionLocale = getLocale(request);
//		if (!possibleLanguageList.isEmpty() && !possibleLanguageList.contains(userOrSessionLocale.getLanguage())) {
//			return new Locale(possibleLanguageList.get(0)); // pozor na null pointer!
//		} else return userOrSessionLocale;
//	}
//
//	public static void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
//		LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
//		if (localeResolver == null) {
//			throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");
//		}
//		localeResolver.setLocale(request, response, locale);
//	}
//
//	public static List<String> getSortedLanguageList(List<String> languageList) {
//		// mam seznam jazyku, seradim ho IMHO blbost!
//		Collections.sort(languageList);
//		// potrebuji zaradit anglictinu na prvni misto
//		// TODO v jakem poradi maji byt jazyky videt?
//
//		// je anglictina v jazycich a neni na zacatku?
//		if (languageList.contains("en") && languageList.indexOf("en") > 0) {
//			languageList.remove("en");// odstran z puvodni
//			languageList.add(0, "en"); // dej na zacatek
//		}
//		return languageList;
//	}
//
//	/**
//	 * Check if this request should be shown in modal tab
//	 * @param request
//	 * @return true only if parameter is present with "true" value
//	 * @deprecated try not to use this, use methods below instead
//	 */
//	@Deprecated
//	public static boolean showInTab(HttpServletRequest request) {
//		//TODO refaktorovat, aby by default byl atribut false (coz vetsinou je) a dat tohle ziskavani do interceptoru
//		String showInTabStr = ServletRequestUtils.getStringParameter(request, "showInTab", "false");
//		boolean showInTab =  ("true".equals(showInTabStr));
//		return showInTab;
//	}
//
//	/**
//	 * Add showInTab param to {@link Model}
//	 *
//	 */
//	public static void showInTab(Model m, HttpServletRequest request) {
//		m.addAttribute("showInTab", RequestUtilities.showInTab(request));
//	}
//
//	/**
//	 * Add showInTab param to {@link Map}
//	 *
//	 */
//	public static void showInTab(Map<String, Object> m, HttpServletRequest request) {
//		m.put("showInTab", RequestUtilities.showInTab(request));
//	}
//
//
//	/**
//	 * Get complete path for absolute links to server, depending on domain of client
//	 * eg. https://www.positrex.eu , or http://localhost/cokoliv  -depending from where client access server
//	 * @param request
//	 * @return
//	 */
//	public static String getAbsoluteServerAddress(HttpServletRequest request) {
//		String scheme = request.getScheme();
//		String serverName = request.getServerName();
//		int serverPort = request.getServerPort();
//		String contextPath = request.getContextPath();  // includes leading forward slash
//
//		return scheme + "://" + serverName + ":" + serverPort + contextPath;
//	}
//
//	/**
//	 * Can be used to access current {@link HttpServletRequest}
//	 * But use only when necessary!!!
//	 * @return
//	 */
//	public static HttpServletRequest getCurrentHttpRequest(){
//		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//		if (requestAttributes instanceof ServletRequestAttributes) {
//			return ((ServletRequestAttributes)requestAttributes).getRequest();
//		}
//		return null;
//	}
//
//	/**
//	 * @return current options from request ,use from validator
//	 */
//	public static SecurityInfo getCurrentSecurityInfo(){
//		OptionsManager optionsManager = SpringContext.getBean(OptionsManager.class);
//		return optionsManager.getOptions(getCurrentHttpRequest());
//	}
//
//	/**
//	 * Reconstruct URI with query string if present
//	 * @param request
//	 * @return
//	 */
//	public static String getCompleteURI(HttpServletRequest request) {
//		return RequestUtils.getCompleteURI(request);
//	}
//
//
//	/**
//	 * Reconstruct URI with query string if present, remove app context (/positrex)
//	 * @param request
//	 * @return
//	 */
//	public static String getCompleteURINormalized(HttpServletRequest request) {
//		String uri = getCompleteURI(request);
//		String pref = getCtxPrefix(request);
//		if (uri.startsWith(pref)) {
//			uri = uri.replace(pref, "");
//		}
//		return uri;
//	}
//	/**
//	 * Get context path if any, feel free to prepend this to any url that begins with /
//	 * @param r
//	 * @return prefix starting with /,but not ending with it, or empty string if app is running in ROOT context, see {@link HttpServletRequest#getContextPath()}
//	 */
//	public static String getCtxPrefix(HttpServletRequest r) {
//		return r.getContextPath();
//	}
//
//	/**
//	 * Simple helper that adds context prefix to base url if not present
//	 * @param request
//	 * @param base
//	 * @return
//	 */
//	public static String getUrlWithPrefix(HttpServletRequest request, String base) {
//		String pref = getCtxPrefix(request);
//		if (base.startsWith(pref)) {
//			return base;
//		}
//		return pref + base;
//	}
//
	/**
	 * Get cookie by name or null
	 * @param req
	 * @param cookieName
	 * @return
	 */
	public static Cookie getCookie(HttpServletRequest req, String cookieName) {
		Cookie[] cookies = req.getCookies();
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (cookieName.equals(c.getName())) {
					return c;
				}
			}
		}
		return null;
	}
	/**
	 * remove cookie from response
	 * @param cookieName
	 * @param response
	 * @param request
	 * @return
	 */
	public static Cookie deleteCookie(String cookieName, HttpServletRequest request, HttpServletResponse response) {
		Cookie cookie = new Cookie(cookieName, null);
		cookie.setMaxAge(0);
		cookie.setPath(StringUtils.hasLength(request.getContextPath()) ? request.getContextPath() : "/");
		cookie.setValue("");
		response.addCookie(cookie);
		return cookie;
	}
//
//
//	/**
//	 * Return true if request ws done by AJAX
//	 *
//	 * @param request
//	 * @return
//	 */
//	public static boolean isAjax(HttpServletRequest request) {
//		String xrequestedWith = request.getHeader("x-requested-with");
//		return xrequestedWith != null && xrequestedWith.equals("XMLHttpRequest");
//	}
//
//	/**
//	 * @see RequestUtils#dumpRequest(HttpServletRequest)
//	 */
//	public static String dumpRequest(HttpServletRequest rq) {
//		return RequestUtils.dumpRequest(rq);
//	}


}

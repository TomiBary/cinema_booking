package cz.cinema_booking.utils;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContext implements BeanFactoryAware {
	private static BeanFactory factory;

	public SpringContext() {
	}

	public void setBeanFactory(BeanFactory beanFactory) {
		factory = beanFactory;
	}

	public static <T> T getBean(String nameBean, Class<T> className) {
		return factory.getBean(nameBean, className);
	}

	public static <T> T getBean(Class<T> className) {
		return factory.getBean(className);
	}
}

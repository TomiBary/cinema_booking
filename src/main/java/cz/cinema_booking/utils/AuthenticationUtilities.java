package cz.cinema_booking.utils;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class AuthenticationUtilities {
	private static final String REMEMBER_ME_COOKIE = "SPRING_SECURITY_REMEMBER_ME_COOKIE";

	public static boolean isUserFromCookie() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication != null && RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
	}
	/**
	 * 
	 * @param request
	 * @param response
	 * @param userLogout true if user wants to be log out, therefore remove rememberme cookie
	 */
	public static void logout(HttpServletRequest request, HttpServletResponse response, boolean userLogout) {
		HttpSession httpSession = request.getSession(false);
		if (httpSession != null) {
			if (userLogout) {
				httpSession.setAttribute("userLogout", true);
				Cookie rememberMe = getRememberMeCookie(request);
				if (rememberMe != null) {
					Cookie cookie = new Cookie(REMEMBER_ME_COOKIE, null);
					cookie.setMaxAge(0);
					cookie.setPath(StringUtils.hasLength(request.getContextPath()) ? request.getContextPath() : "/");
					response.addCookie(cookie);
				}
			}
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth);
			}
			SecurityContextHolder.getContext().setAuthentication(null);
		}
	}
	
	/**
	 * Find spring remember me cookie 
	 * @param request
	 * @return cookie or null
	 */
	private static Cookie getRememberMeCookie(HttpServletRequest request) {
		return RequestUtilities.getCookie(request, REMEMBER_ME_COOKIE);
	}
	
	
}

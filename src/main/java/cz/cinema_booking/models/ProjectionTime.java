package cz.cinema_booking.models;

import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Collection;
import java.util.Date;

@Data
@Entity(name = "projection_time")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProjectionTime extends AbstractEntity {

  @Nonnull
  @ManyToOne
  @JoinColumn(name = "movie_id", nullable = false)
  private Movie movie;

  @Temporal(TemporalType.TIMESTAMP)
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
  private Date projectionDateTime;

  @ManyToOne
  @JoinColumn(name = "hall_id", nullable = false)
  private Hall hall;

  @OneToMany(mappedBy = "projectionTime", cascade = CascadeType.ALL)
  private Collection<Reservation> reservations;

  @Override
  public String toString() {
    return "ProjectionTime{" +
        "movie=" + movie +
        ", projectionDateTime=" + projectionDateTime +
        ", hall=" + hall +
        ", reservations=" + reservations +
        '}';
  }
}

package cz.cinema_booking.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationForm {
    private int projectionId;
    private List<SeatHelper> seatHelperList;

    public Reservation toReservation(){
        return new Reservation(new ProjectionTime().withId(projectionId));
    }

    public void withOnlyEnabled(){
        this.seatHelperList = seatHelperList.stream().filter(s -> s.isEnabled()).collect(Collectors.toList());
    }

    public Collection<Seat> getSeats(){
        return seatHelperList.stream().map(sh -> (Seat) new Seat().withId(sh.getId())).collect(Collectors.toList());
    }
}

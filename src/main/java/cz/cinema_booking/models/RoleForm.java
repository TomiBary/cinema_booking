package cz.cinema_booking.models;

import lombok.Data;

import java.util.List;

@Data
public class RoleForm {
    private int userId;
    private List<RoleHelper> roleHelperList;
}

package cz.cinema_booking.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.Transient;

import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Role extends AbstractEntity implements GrantedAuthority {
  public static final Role USER = new Role(RoleType.USER);
  public static final Role ADMIN = new Role(RoleType.ADMIN);
  @Getter
  @Enumerated(EnumType.STRING)
  @Column(unique = true, nullable = false)
  private RoleType type;
  @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
  private Collection<SimpleUser> users;

  public Role(RoleType roleType) {
    this.type = roleType;
  }

  @Override
  public String getAuthority() {
    return type.getAuthority();
  }

  public String getRoleName() {
    return type.name();
  }

  @Override
  public String toString() {
    return "Role{" +
        "type=" + type +
        '}';
  }

  @Transient
  public enum RoleType {
    //        DUMMY(-1),
    ADMIN(0),
    USER(1);

    public static final String ROLE_PREFIX = "ROLE_";
    private byte id;

    private RoleType(int id) {
      this.id = (byte) id;
    }

    private static String prefix(String role) {
      return ROLE_PREFIX + role;
    }

    public Integer getId() {
      return Integer.valueOf(this.id);
    }

    public String getAuthority() {
      return this.auth();
    }

    private String auth() {
      return prefix(this.name());
    }

  }

}



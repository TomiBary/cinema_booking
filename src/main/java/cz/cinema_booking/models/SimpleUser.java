package cz.cinema_booking.models;


import cz.cinema_booking.validation.annotations.PasswordMatch;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;

@Data
@Entity(name = "users")
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@PasswordMatch
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class SimpleUser extends AbstractEntity {
	//UserData -> bcs User entity could not be created in DB

	@NotNull
	@Size(min = 5, max = 32)
	@Column(nullable = false, unique = true)
	@Pattern(regexp = "^[a-zA-Z0-9_]*$")
	protected String username;

	@Email
	protected String emailAddress;

	@NotNull(message = "Heslo nesmi být prázdné")
	@Size(min = 8, max = 256)
	@Pattern(regexp = "^(?=.*\\d)(?=.*[a-zA-Z]).{8,36}$", message = "Heslo musí obsahovat čísla i písmena")
	@Column(nullable = false)
	private String password;

	@Transient
	private String matchingPassword;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role",
			joinColumns = {@JoinColumn(name = "user_id")},
			inverseJoinColumns = {@JoinColumn(name = "role_id")})
	private Collection<Role> roles;

//    private Date createdDate;

	public String getEmail() {
		return emailAddress;
	}

	public void encodePassword(PasswordEncoder encoder) {
		password = encoder.encode(password);
	}

	public SimpleUser withRole(Role role) {
		if (roles == null)
			this.roles = new ArrayList<>();
		if (!this.roles.contains(role))
			this.roles.add(role);
		return this;
	}

	public SimpleUser withoutRole(Role role) {
		if (roles != null && !roles.isEmpty())
			this.roles.remove(role);
		return this;
	}

}

package cz.cinema_booking.models;

import jakarta.annotation.Nonnull;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Entity
@NoArgsConstructor
public class Hall extends AbstractEntity {
    //TODO vyřešit řady, sloupce, dvojsedačky, místa pro postiž   - možná by stačilo udělat jen mapku někde v malování nebo kdekoliv, neřešit řady a sloupce, ale bude tam jen napsané číslo sedačky od 1 do 100 a uživatel si jen vybere to číslo.. takže vůbec nemusíme řešit, jaká to je řada a sloupec (bylo by to zlehčení)
    // vyresit jako 2d grid, A-Z / 1-99 mista kde se neda sedet jako nepristupna

    //TODO pro admina, definovat si vzhled sálu
    // 1) pridat sal
    // 2) definovat si sedačky v sále

    @Nonnull
    @Column(unique = true, nullable = false)
    private String name;

    @OneToMany(mappedBy = "hall")
    private Collection<Seat> seats;

    @OneToMany(mappedBy = "hall")
    private Collection<ProjectionTime> projectionTimes;

    public Hall(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "name='" + name + '\'' +
                ", seats=" + seats +
                '}';
    }
}

package cz.cinema_booking.models;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Entity
@Table(
    uniqueConstraints =
    @UniqueConstraint(columnNames = {"hall_id", "row", "col"})
)
@NoArgsConstructor
public class Seat extends AbstractEntity {

  @Valid
  private Coordinates coords;

  @ManyToOne
  @JoinColumn(name = "hall_id", nullable = false)
  private Hall hall;

  @ManyToMany(mappedBy = "seats")
  private Collection<Reservation> reservations;

  public Seat(String name, int row, int col, int hallId) {
    this.hall = new Hall().withId(hallId);
    this.coords = new Coordinates(name, row, col);
  }

  public static Map<Integer, List<Seat>> getSeatMap(Collection<Seat> seats) {
    return seats.stream().sorted(Comparator.comparingInt(Seat::getCol)).collect(Collectors.groupingBy(Seat::getRow, Collectors.toList()));
  }

  public String getName() {
    return coords.getName();
  }

  public int getRow() {
    return coords.getRow();
  }

  public int getCol() {
    return coords.getColumn();
  }

  @Override
  public String toString() {
    return "Seat{" +
        "coords=" + coords +
        ", reservations=" + reservations +
        '}';
  }
}

package cz.cinema_booking.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Entity
//TODO implement just one users registration on projection time
//@Table(
//    uniqueConstraints =
//    @UniqueConstraint(columnNames = {"user_id", "projection_time_id"})
//)
@NoArgsConstructor
public class Reservation extends AbstractEntity {

    @ManyToMany
    @JoinTable(name = "reservation_seat",
            joinColumns = {@JoinColumn(name = "reservation_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "seat_id", nullable = false)})
    private Collection<Seat> seats;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private SimpleUser owner;  // koho je to rezervace

    @ManyToOne
    @JoinColumn(name = "projection_time_id", nullable = false)
    private ProjectionTime projectionTime;//  - datum na kdy je rezervace?

    public Reservation(ProjectionTime projectionTime) {
        this.projectionTime = projectionTime;
    }

    public Reservation withOwnerId(long id){
        this.owner = new SimpleUser().withId(id);
        return this;
    }

    public Reservation withSeats(Collection<Seat> seats){
        this.seats = seats;
        return this;
    }

    @Override
    public String toString() {
        return "Reservation{}";
    }
}

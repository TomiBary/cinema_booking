package cz.cinema_booking.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeatHelper {
    private long id;
    private String name;
    private long boxId;
    private boolean enabled;
}

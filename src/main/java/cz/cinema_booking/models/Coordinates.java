package cz.cinema_booking.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Coordinates {

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private int row;

    @Column(name = "col", nullable = false)
    private int column;

    //TODO return Optional
    public static String getCharForNumber(int i) {
        return i > 0 && i < 27 ? String.valueOf((char)(i + 64)) : null;
    }

}

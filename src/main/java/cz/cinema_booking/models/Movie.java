package cz.cinema_booking.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import java.util.Collection;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Movie extends AbstractEntity {

    @NonNull
    @NotBlank
    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private int durationTimeSec; // v minutách nebo třeba vteřinách (pak jde snadno převést na cokoliv chceme)

    @NonNull
    @Column
    private String description;

    @NonNull
    @Column
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date releaseDate;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Collection<ProjectionTime> projectionTimes;


    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
            ", durationTimeSec=" + durationTimeSec +
                ", description='" + description + '\'' +
                ", releaseDate=" + releaseDate +
                '}';
    }
}

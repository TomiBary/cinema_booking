package cz.cinema_booking.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Data
@Entity(name = "review_article")
@ToString
@EqualsAndHashCode(callSuper = true)
public class Review extends AbstractEntity {

    @NotNull
    @Size(min = 1, max = 128)
    @Column(unique = true, nullable = false)
    private String header;

    @NotNull
    @Size(min = 1)
    @Column(nullable = false, columnDefinition = "TEXT")
    private String reviewText;

    @Min(value = 0, message = "Review can be min 0")
    @Max(value = 100, message = "Review can be max 100")
    @NotNull
    @Column(nullable = false)
    private int reviewScore;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private SimpleUser author;

    @CreationTimestamp
    @Temporal(TemporalType.DATE)
    private Date created;

}

package cz.cinema_booking.models;

import lombok.Data;

@Data
public class RoleHelper {
		private int roleId;
		private boolean enabled;
}

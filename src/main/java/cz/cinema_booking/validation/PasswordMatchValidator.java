package cz.cinema_booking.validation;

import cz.cinema_booking.models.SimpleUser;
import cz.cinema_booking.validation.annotations.PasswordMatch;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, Object> {

  @Override
  public void initialize(final PasswordMatch constraintAnnotation) {
    //noop
  }

  @Override
  public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
    final SimpleUser user = (SimpleUser) obj;
    return user.getPassword().equals(user.getMatchingPassword());
  }

}
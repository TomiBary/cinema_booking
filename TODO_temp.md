# Project TODO

[![Build Status](https://travis-ci.org/spring-projects/spring-data-examples.svg?branch=issue%2F%2313)](https://travis-ci.org/spring-projects/spring-data-examples)

1) Dat pozor na transakce !!! [docs](https://www.logicbig.com/tutorials/spring-framework/spring-data/transactions.html)
2) Repository methods [Repository dokumentace](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)
3) Dockerizovat projekt [Toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/) / [Docker Spring](https://spring.io/guides/gs/spring-boot-docker/)

###TODO
 - [x] Zprovoznit přihlašování a registraci [Registrace](https://www.baeldung.com/registration-with-spring-mvc-and-spring-security)
 - [x] Nachystat repository pro ziskavani objektu z DB 
 - [x] Vyresit role [Thymeleaf object w/list](https://www.baeldung.com/thymeleaf-list)
 - [x] Zrušit role z DB nebo znemoznit prodavani duplikaci do Role table
 - [ ] Upravit update metody controllerů - teď je tam save -> fixnout upserty v HTML, protoze tam nemame hidden ID a proto se vytvari nove entity
 - [ ] Upravit UI role
 - [ ] Kontrola [modelovych trid](src/main/java/cz/cinema_booking/models/)
 - [ ] Dockerizovat projekt
    1) MOZNA UDELAT CO NEJDRIVE -> umozni nam si posilat image soubor se stejnou DB 

###Potential TODOs / Implementations
1) Rezervace neprihlaseneho/neregistrovaneho uzivatele
2) Pridat komentare
3) Pridat obrazky k filmum
4) EmailService [Spring-email Baeldung](https://www.baeldung.com/spring-email) [repo](https://github.com/Baeldung/spring-security-registration/blob/master/src/main/java/org/baeldung/web/controller/RegistrationController.java)
5) DefaultHandlerExceptionResolver
6) Opravneni operaci
7) Metody controlleru omezit podle opravnění (řešeno pouze ve View a [WebSecConfig](src/main/java/cz/cinema_booking/security/WebSecurityConfig.java))
8) Implementovat csrf protekci
9) Po zmene role nacist znovu usera pro aktualizaci roli
10) Po registraci automaticky prihlasit
11) Nahradit repos. servisní vrstvou

##Interesting info
EmailAddress specialni pripad - > vlastni trida ale stejna tabulka
Správa rolí

###Dotazy
 - [x] *Ziskavani vlastniho potomka od Principal v metode controlleru?*
 - [ ] *Zeptat se jak řešit inserty atp pomocí modalu - positrex*
 - [ ] *Zjistit jak řešit uživatelovy role - ukladani (pri ulozeni uzivatele s rolemi se pridaji zaznamy i do tabulky **role**)*
 - [ ] *Zjistit jak napsat Query na ziskani volnych sedacek pro promitani*
 - [ ] *Proc pada Review pri pridani kdyz se da score napr 120, měli by ho zachytit validace*

####Zajimavé linky
- [Audit Entities (Entity s created)](https://blog.octo.com/audit-with-jpa-creation-and-update-date/)
- [Audit model spring-boot-jpa-hibernate-postgresql-restful-crud-api-example](https://www.callicoder.com/spring-boot-jpa-hibernate-postgresql-restful-crud-api-example/)
- [Configs](https://github.com/spring-projects/spring-data-book/tree/master/jpa/src/main/java/com/oreilly/springdata/jpa)
- [Spring - JPA](https://github.com/spring-projects/spring-data-examples/tree/master/jpa/example/src/main/java/example/springdata)
- [Hibernate ORM search example](https://www.boraji.com/hibernate-orm-5-hibernate-search-integration-example)
- [Postgres JSONB with hibernate](https://thoughts-on-java.org/persist-postgresqls-jsonb-data-type-hibernate/)
- [EntityManager](https://www.baeldung.com/hibernate-entitymanager)
- [Controller method parameter & type converters](https://www.petrikainulainen.net/programming/spring-framework/spring-from-the-trenches-creating-a-custom-handlermethodargumentresolver/)
- [Thymeleaf conditionals](https://www.baeldung.com/spring-thymeleaf-conditionals)